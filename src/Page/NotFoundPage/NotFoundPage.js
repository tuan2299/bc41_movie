import React from "react";
import { NavLink } from "react-router-dom";

export default function NotFoundPage() {
  return (
    <div className="container flex justify-center">
      <h2 className="text-2xl font-black text-red-500 animate-pulse">
        NotFoundPage
      </h2>
    </div>
  );
}
