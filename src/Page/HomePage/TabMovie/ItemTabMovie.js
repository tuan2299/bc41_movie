import moment from "moment/moment";
import React from "react";

export default function ItemTabMovie({ phim }) {
  return (
    <div className="flex items-center pb-3 space-x-5 border-b border-black justify-evenly">
      <img
        className="object-cover object-top h-48 rounded w-28"
        src={phim.hinhAnh}
        alt=""
      />
      <div>
        <h5 className="mb-5 text-xl font-medium">{phim.tenPhim}</h5>
        <div className="grid grid-cols-3 gap-5">
          {phim.lstLichChieuTheoPhim.map((item) => {
            return (
              <span className="p-3 text-white bg-red-500 rounded">
                {moment(item.ngayChieuGioChieu).format("DD-mm-yyyy ~ hh-mm")}
              </span>
            );
          })}
        </div>
      </div>
    </div>
  );
}
