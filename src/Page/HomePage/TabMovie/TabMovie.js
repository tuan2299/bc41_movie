import React, { useState } from "react";
import { movieServ } from "../../../service/movieService";
import { Tabs } from "antd";
import ItemTabMovie from "./ItemTabMovie";

const onChange = (key) => {
  console.log(key);
};
const items = [
  {
    key: "1",
    label: `Tab 1`,
    children: `Content of Tab Pane 1`,
  },
];
export default function TabMovie() {
  const [danhSachHeThongRap, setDanhSachHeThongRap] = useState([]);
  useState(() => {
    movieServ
      .getMovieByTheater()
      .then((res) => {
        setDanhSachHeThongRap(res.data.content);
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  let renderHeThongRap = () => {
    return danhSachHeThongRap.map((rap) => {
      return {
        key: rap.maHeThongRap,
        label: <img className="h-16" src={rap.logo} alt="" />,
        children: (
          <Tabs
            style={{ height: 700 }}
            tabPosition="left"
            defaultActiveKey="1"
            items={rap.lstCumRap.map((cumRap) => {
              return {
                key: cumRap.maCumRap,
                label: (
                  <div className="truncate w-60">
                    <p className="font-medium">{cumRap.tenCumRap}</p>
                    <p className="text-xs text-gray-600">{cumRap.diaChi}</p>
                  </div>
                ),
                children: (
                  <div
                    style={{ height: 700, overflow: "scroll" }}
                    className="space-y-5">
                    {cumRap.danhSachPhim.map((phim) => {
                      return <ItemTabMovie phim={phim} />;
                    })}
                  </div>
                ),
              };
            })}
            onChange={onChange}
          />
        ),
      };
    });
  };
  return (
    <div className="container">
      {" "}
      <Tabs
        style={{ height: 700 }}
        tabPosition="left"
        defaultActiveKey="1"
        items={renderHeThongRap()}
        onChange={onChange}
      />
    </div>
  );
}
