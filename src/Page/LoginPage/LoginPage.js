import { Button, Checkbox, Form, Input, message } from "antd";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { SET_USER_LOGIN } from "../../redux/contant/userContant";
import { userServ } from "../../service/userService";
import { localUserServ } from "../../service/localService";
import Lottie from "lottie-react";
import bg_animate from "../../asset/animate_login.json";

const LoginPage = () => {
  let navigate = useNavigate();
  let dispatch = useDispatch();
  const onFinish = (values) => {
    userServ
      .login(values)
      .then((res) => {
        message.success("Đăng nhập thành công.");
        // đưa thông tin lên redux
        dispatch({
          type: SET_USER_LOGIN,
          payload: res.data.content,
        });
        // lưu xuống local
        localUserServ.set(res.data.content);
        // chuyển hướng user về homepage
        navigate("/");
        console.log(res);
      })
      .catch((err) => {
        message.error("Đăng nhập thất bại.");
        console.log(err);
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div className="flex items-center justify-center w-screen h-screen p-20 bg-orange-400">
      <div className="container flex p-20 bg-white rounded-lg">
        <div className="w-1/2 h-full">
          <Lottie animationData={bg_animate} loop={true} />
        </div>
        <div className="w-1/2 h-full">
          <h2 className="text-4xl font-bold text-center">Login</h2>
          <Form
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 24,
            }}
            style={{
              width: "100%",
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            layout="vertical">
            <Form.Item
              label="Username"
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Please input your username!",
                },
              ]}>
              <Input />
            </Form.Item>
            <Form.Item
              label="Password"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
              ]}>
              <Input.Password />
            </Form.Item>
            <Form.Item
              wrapperCol={{
                span: 24,
              }}
              className="flex items-center justify-center">
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
};
export default LoginPage;
