import { Progress } from "antd";
import React, { useEffect, useState } from "react";
import { NavLink, useParams } from "react-router-dom";
import { movieServ } from "../../service/movieService";

export default function DetailPage() {
  let params = useParams();
  const [movie, setMovie] = useState({});
  useEffect(() => {
    // movieServ
    //   .getDetailMovie(params.id)
    //   .then((res) => {
    //     console.log(res.data.content);
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //   });
    let fetchDetail = async () => {
      try {
        let res = await movieServ.getDetailMovie(params.id);
        console.log(`🚀 -> fetchDetail -> res:`, res);
        setMovie(res.data.content);
        //
      } catch (error) {}
    };
    fetchDetail();
  }, []);
  return (
    <div className="container flex ">
      <img className="w-1/4" src={movie.hinhAnh} alt="" />
      <div className="p-5 space-y-10">
        <h2 className="text-3xl font-bold">{movie.tenPhim}</h2>
        <p>{movie.moTa}</p>
        <Progress
          strokeColor={{
            "0%": "#108ee9",
            "100%": "#87d068",
          }}
          type="circle"
          percent={movie.danhGia * 10}
          format={(percent) => `${percent / 10} Diem`}
        />
        <NavLink
          to={`/booking/${movie.maPhim}`}
          className="px-5 py-2 text-white bg-red-500 rounded">
          Mua ve
        </NavLink>
      </div>
    </div>
  );
}
