import React from "react";
import { NavLink } from "react-router-dom";
import UserMenu from "./UserMenu";

export default function HeaderDestop() {
  return (
    <div className="h-20 shadow">
      <div className="container flex items-center justify-between h-full mx-auto">
        <NavLink to={"/"}>
          <span className="text-2xl font-bold text-red-500 animate-pulse">
            CyberFlix
          </span>
        </NavLink>
        <UserMenu />
      </div>
    </div>
  );
}
