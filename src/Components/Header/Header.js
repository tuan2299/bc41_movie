import React from "react";
import { Desktop, Mobile, Tablet } from "../../Layout/Responsive";
import HeaderDestop from "./HeaderDestop";
import HeaderMobile from "./HeaderMobile";
import HeaderTablet from "./HeaderTablet";

export default function Header() {
  return (
    <div>
      <Desktop>
        <HeaderDestop />
      </Desktop>
      <Tablet>
        <HeaderTablet />
      </Tablet>
      <Mobile>
        <HeaderMobile />
      </Mobile>
    </div>
  );
}
