import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { localUserServ } from "../../service/localService";

export default function UserMenu() {
  let userInfor = useSelector((state) => {
    return state.userReducer.userInfor;
  });
  let handleLogout = () => {
    localUserServ.remove();
    // window.location.href = "/login";
    window.location.reload();
  };
  let renderContent = () => {
    if (userInfor) {
      return (
        <>
          <span>{userInfor.hoTen}</span>
          <button
            onClick={handleLogout}
            className="px-5 py-2 border-2 border-black rounded">
            Đăng xuất
          </button>
        </>
      );
    } else {
      return (
        <>
          <NavLink to={"/login"}>
            <button className="px-5 py-2 border-2 border-black rounded">
              Đăng Nhập
            </button>
          </NavLink>
          <button className="px-5 py-2 border-2 border-black rounded">
            Đăng Ký
          </button>
        </>
      );
    }
  };
  return <div className="space-x-5">{renderContent()}</div>;
}
