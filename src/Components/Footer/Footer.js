import React from "react";

export default function Footer() {
  return (
    <div className="h-20 shadow">
      <div className="container ">
        <h3 className="text-blue-700">Footer</h3>
      </div>
    </div>
  );
}
